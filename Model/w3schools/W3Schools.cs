﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Model.w3schools
{
    public class W3Schools : SpecFlowPage, ILoad
    {
        [FindsBy(How = How.Id, Using = "customers")] public Table CustomersTable;

        public W3Schools() : base(new BaseDriver(), "https://www.w3schools.com/html/html_tables.asp")
        {
        }

        public bool IsLoaded() => CustomersTable[0].IsOnPage;
    }
}
