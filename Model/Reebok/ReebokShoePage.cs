﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Model.Reebok
{
    class ReebokShoePage : SpecFlowPage, ILoad
    {
        [FindsBy(How = How.XPath, Using = "//*[@type = 'submit']")] private Button AddToCart;
        [FindsBy(How = How.XPath, Using = "//div[@class = 'square-list']/ul/li")] private IList<Button> Sizes;

        public ReebokShoePage(BaseDriver Driver) : base(Driver)
        {
        }

        public bool IsLoaded() => AddToCart.IsOnPage;
    }
}
