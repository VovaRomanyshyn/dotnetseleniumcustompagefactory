﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Model.Reebok
{
    public class ReebokMainPage : SpecFlowPage, ILoad
    {
        
        [FindsBy(How = How.XPath, Using = "//*[@class = 'ui-dialog-titlebar-close ui-corner-all']")] private Button CloseDialog;
        [FindsBy(How = How.Id, Using = "truste-consent-button")] private Button CloseCookies;
        [FindsBy(How = How.XPath, Using = "//a[@manual_cm_sp = 'header-_-MĘŻCZYŹNI']")] private Button Men;
        [FindsBy(How = How.XPath, Using = "//a[@manual_cm_sp='header-_-mężczyźni-_-buty']/../../following-sibling::node()/a")] private IList<Button> MenShoes;
        [FindsBy(How = How.XPath, Using = "//a[@manual_cm_sp = 'header-_-KOBIETY']")] private Button Women;

        public ReebokMainPage(BaseDriver Driver) : base(Driver)
        {
        }

        public ReebokMainPage() : base(new BaseDriver(), "https://www.reebok.pl/")
        {
        }

        public bool IsLoaded() => Men.IsOnPage && Women.IsOnPage;

    }
}
