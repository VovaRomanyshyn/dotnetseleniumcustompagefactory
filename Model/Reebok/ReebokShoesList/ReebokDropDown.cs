﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace Model.Reebok
{
    public class ReebokDropDown : SubPage, IDropDown
    {
        [FindsBy(How = How.XPath, Using = "div//ul/li/span[@data-val]")] private IList<Button> Options;
        [FindsBy(How = How.XPath, Using = "div/div/a/span")] private BaseElement SelectedOption;
        [FindsBy(How = How.XPath, Using = "//div[@id = 'loading-overlay-spinner']")] private BaseElement Loading;


        public ReebokDropDown(IWebElement Element, BaseDriver Driver, string Name) : base(Element, Driver, Name)
        {
        }



        public void ClickByText(string text)
        {
            if (!IsValueSelected(text))
            {
                CastTo<Button>().Click();
                foreach (Button button in Options)
                {
                    if (button.ToString().Equals(text))
                    {
                        button.Click();
                        Loading.WaitUntilElementDissapear();
                        break;
                    }
                }
            }
        }

        public bool IsValueSelected(string value) => SelectedOption.ToString().Equals(value);

        public void SelectAnyOption()
        {
            CastTo<Button>().Click();
            Options[new Random().Next(Options.Count)].Click();
        }
    }
}
