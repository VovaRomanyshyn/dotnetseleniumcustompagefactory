﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.Reebok;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Model.OutlookMailModel.ReebokShoesList
{
    public class ReebokShoesListPage : SpecFlowPage, ILoad
    {
        [FindsBy(How = How.XPath, Using = "(//div[@class = 'sort-products'])[1]")] private ReebokDropDown SortBy;
        [FindsBy(How = How.ClassName, Using = "paging-select-wrapper")] private ReebokDropDown PageNumberDropDown;
        [FindsBy(How = How.XPath, Using = "//*[@class = 'show lazyload']/..")] private IList<Button> ShoesList;
        [FindsBy(How = How.XPath, Using = "//*[@class = 'ui-dialog-titlebar-close ui-corner-all']")] private Button DialogButton;


        public ReebokShoesListPage(BaseDriver Driver) : base(Driver)
        {
            if (DialogButton.IsVisible())
            {
                DialogButton.Click();
            }
        }

        public bool IsLoaded() => ShoesList.Count > 0;
    }
}
