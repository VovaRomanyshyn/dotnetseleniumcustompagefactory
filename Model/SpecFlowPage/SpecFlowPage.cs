﻿using Framework.Driver;
using Framework.Pages;

namespace Model.SpecFlowPages
{
    public class SpecFlowPage : BasePage
    {
        public SpecFlowPage(BaseDriver Driver) : base(Driver)
        {
        }

        public SpecFlowPage(BaseDriver Driver, string Url) : base(Driver, Url)
        {
        }
    }
}
