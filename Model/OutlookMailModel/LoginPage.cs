﻿using System;
using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;

namespace Model.OutlookMailModel
{
    public class LoginPage : SpecFlowPage, ILoad, ILogin
    {
        [FindsBy(How = How.XPath, Using = "//*[@class = 'linkButtonFixedHeader office-signIn']")] private Button SignIn;
        [FindsBy(How = How.XPath, Using = "//input[@type = 'email']")] private TextInputField Email;
        [FindsBy(How = How.XPath, Using = "//input[@type = 'submit']")] private Button Submit;
        [FindsBy(How = How.XPath, Using = "//input[@type = 'password']")] private TextInputField Password;

        public LoginPage() : base(new BaseDriver(), "https://outlook.live.com/")
        {
        }

        public LoginPage(BaseDriver Driver) : base(Driver, "https://outlook.live.com/")
        {
        }

        public bool IsLoaded() => SignIn.IsOnPage;

        public IPage CorrectLogin(string Login, string Pass)
        {
            SignIn.Click();
            Email.SendString(Login);
            Submit.Click();
            Password.SendString(Pass);
            Submit.Click();
            return Page<MainMailPage>();
        }

        public IPage UnCorrectLogin(string Login, string Pass)
        {
            SignIn.Click();
            Email.SendString(Login);
            Submit.Click();
            Password.SendString(Pass);
            Submit.Click();
            return this;
        }
    }
}
