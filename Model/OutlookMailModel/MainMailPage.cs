﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;

namespace Model.OutlookMailModel
{
    public class MainMailPage : SpecFlowPage, ILoad
    {
        [FindsBy(How = How.XPath, Using = "//button//*[text() = 'New message']")] private Button NewMessage;

        public MainMailPage(BaseDriver Driver) : base(Driver)
        {
        }

        public bool IsLoaded() => NewMessage.IsOnPage;
    }
}
