﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Model.Adidas
{
    public class AdidasMainPage : SpecFlowPage, ILoad
    {
        [FindsBy(How = How.Id, Using = "truste-consent-button")] private Button CloseCookies;
        [FindsBy(How = How.XPath, Using = "//a[@manual_cm_sp = 'header-_-MĘŻCZYŹNI']")] private Button Men;
        [FindsBy(How = How.XPath, Using = "//a[@manual_cm_sp='header-_-mężczyźni-_-buty']/../following-sibling::node()/li/a")] private IList<Button> MenShoes;
        [FindsBy(How = How.XPath, Using = "//a[@manual_cm_sp = 'header-_-KOBIETY']")] private Button Women;

        public AdidasMainPage(BaseDriver Driver) : base(Driver)
        {
        }

        public AdidasMainPage() : base(new BaseDriver(), "https://www.adidas.pl/")
        {
        }

        public bool IsLoaded() => Men.IsOnPage && Women.IsOnPage;
    }
}
