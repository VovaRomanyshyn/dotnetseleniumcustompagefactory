﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.Adidas.AdidasShoesList;
using Model.SpecFlowPages;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Model.Adidas
{
    class AdidasShoesListPage : SpecFlowPage, ILoad
    {
        [FindsBy(How = How.XPath, Using = "//div[@data-auto-id = 'plp-sort-by-dropdown']")] private AdidasDropDown SortBy;
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'gl-dropdown gl-dropdown--') and contains(@class ,'small')]")] private AdidasDropDown PageNumberDropDown;
        [FindsBy(How = How.XPath, Using = "//a[@data-auto-id = 'glass-hockeycard-link']")] private IList<Button> ShoesList;

        public AdidasShoesListPage(BaseDriver Driver) : base(Driver)
        {
        }

        public bool IsLoaded() => ShoesList.Count > 0 && SortBy.IsOnPage && PageNumberDropDown.IsOnPage;
    }
}
