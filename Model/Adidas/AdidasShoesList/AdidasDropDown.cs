﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace Model.Adidas.AdidasShoesList
{
    class AdidasDropDown : SubPage, IDropDown
    {
        [FindsBy(How = How.XPath, Using = "div/ul/li")] private IList<Button> Options;
        [FindsBy(How = How.XPath, Using = "button/span")] private BaseElement SelectedOption;


        public AdidasDropDown(IWebElement Element, BaseDriver Driver, string Name) : base(Element, Driver, Name)
        {
        }

        public void ClickByText(string text)
        {
            if (!IsValueSelected(text))
            {
                CastTo<Button>().Click();
                foreach (Button button in Options)
                {
                    if (button.ToString().Equals(text))
                    {
                        button.Click();
                        break;
                    }
                }
            }
        }

        public bool IsValueSelected(string value) => SelectedOption.VisibleString().Equals(value);

        public void SelectAnyOption()
        {
            CastTo<Button>().Click();
            Options[new Random().Next(Options.Count)].Click();
        }
    }
}
