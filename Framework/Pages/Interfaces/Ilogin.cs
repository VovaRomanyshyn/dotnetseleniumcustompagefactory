﻿namespace Framework.Pages
{
    public interface ILogin : IPage
    {
        IPage CorrectLogin(string Login, string Pass);
        IPage UnCorrectLogin(string Login, string Pass);
    }
}
