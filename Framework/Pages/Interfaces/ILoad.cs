﻿namespace Framework.Pages
{
    public interface ILoad : IPage
    {
        bool IsLoaded();
    }
}
