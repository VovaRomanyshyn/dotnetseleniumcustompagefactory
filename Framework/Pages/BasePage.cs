﻿using Framework.Driver;
using Framework.PageDecorators;
using Framework.WebElements;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Framework.Pages
{
    public class BasePage : AbstractPage, IPage
    {
        public BasePage(BaseDriver Driver) : base(Driver)
        {
            IPageObjectMemberDecorator pageObjectMemberDecorator = new CustomPageObjectMemberDecorator(_driver);
            PageFactory.InitElements(this, _driver.RetryingElementLocator, pageObjectMemberDecorator);
        }

        public BasePage(BaseDriver Driver, string Url) : base(Driver)
        {
            _driver.NavigateTo(Url);
            IPageObjectMemberDecorator pageObjectMemberDecorator = new CustomPageObjectMemberDecorator(_driver);
            PageFactory.InitElements(this, _driver.RetryingElementLocator, pageObjectMemberDecorator);
            _logger.Info($"Driver open page: {Url}");
        }

        public TPage Page<TPage>() where TPage : IPage
        {
            _logger.Info("Driver on page: " + typeof(TPage).Name);
            return (TPage)typeof(TPage).GetConstructor(new[] { typeof(BaseDriver) }).Invoke(new object[] { _driver });
        }

        public TElement ElementWithText<TElement>(string text) where TElement : IElement
        {
            By by = By.XPath($"//*[text() = '{text}']");
            return ElementBy<TElement>(by);
        }

        public TElement ElementWithTextContaints<TElement>(string text) where TElement : IElement
        {
            By by = By.XPath($"//*[contains(text(),'{text}')]");
            return ElementBy<TElement>(by);
        }

        public void ClickButtonByText(string text) => ElementWithText<Button>(text).Click();

        private TElement ElementBy<TElement>(By by) where TElement : IElement
        {
            var WebElement = _driver.FindElement(by);
            if (WebElement == null)
            {
                _logger.Info("Unable to locate : " + typeof(IElement).ToString() + " element using: " + by.ToString());
                return default(TElement);
            }
            else
                return (TElement)typeof(TElement)
                   .GetConstructor(new[] { typeof(IWebElement), typeof(BaseDriver), typeof(string) })
                   .Invoke(new object[] { WebElement, _driver, by.ToString() });
        }
    }
}
