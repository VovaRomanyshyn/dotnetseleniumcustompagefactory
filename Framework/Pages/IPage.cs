﻿using Framework.Driver;
using Framework.WebElements;

namespace Framework.Pages
{
    public interface IPage
    {
        string Name();
        BaseDriver BaseDriver();
        TElement ElementWithText<TElement>(string text) where TElement : IElement;
        TElement ElementWithTextContaints<TElement>(string text) where TElement : IElement;
    }
}
