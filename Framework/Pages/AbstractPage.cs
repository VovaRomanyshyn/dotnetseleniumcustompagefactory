﻿using Framework.Driver;
using Framework.WebElements;
using NLog;
using System;

namespace Framework.Pages
{
    public class AbstractPage
    {

        protected BaseDriver _driver;
        private readonly BaseElement _element;
        protected Logger _logger;

        public AbstractPage(BaseDriver Driver)
        {
            _driver = Driver;
            _logger = Driver.Logger;
        }

        public AbstractPage(BaseElement Element)
        {
            _element = Element;
            _driver = _element.Driver;
        }

        public string Name() => GetType().Name;

        public BaseDriver BaseDriver() => _driver;
    }
}
