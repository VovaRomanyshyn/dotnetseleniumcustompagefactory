﻿using Framework.Driver;
using Framework.PageDecorators;
using Framework.WebElements;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Framework.Pages
{
    public class SubPage : BaseElement, ISubPage
    {
        public SubPage(IWebElement Element, BaseDriver Driver, string Name) : base(Element, Driver, Name)
        {
            PageFactory.InitElements(this, new RetryingElementLocator(_element, Settings.TimeWait), new CustomPageObjectMemberDecorator(_driver));
        }
    }
}
