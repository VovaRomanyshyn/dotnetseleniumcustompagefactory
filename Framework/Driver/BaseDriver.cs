﻿using Framework.Waiter;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using LogLevel = NLog.LogLevel;

namespace Framework.Driver
{
    public class BaseDriver
    {
        public IWebDriver Driver { get; private set; }
        public WebWaiter Waiter { get; private set; }
        public Actions Actions { get; private set; }
        public Logger Logger { get; private set; }

        public BaseDriver()
        {
            Driver = new DriverFactory().GetDriver(Settings.Browser);
            Waiter = new WebWaiter(new WebDriverWait(Driver, Settings.TimeWait));
            Actions = new Actions(Driver);
            Logger = LogManager.GetCurrentClassLogger();


            var config = new NLog.Config.LoggingConfiguration();
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            LogManager.Configuration = config;
        }


        public void NavigateTo(string url) => Driver.Navigate().GoToUrl(url);

        public string GetTitle() => Driver.Title;

        public RetryingElementLocator RetryingElementLocator => new RetryingElementLocator(Driver, Settings.TimeWait);

        public IWebElement FindElement(By by)
        {
            bool ElementOnPage = Waiter.UntilElementAppearOnPage(by);
            return ElementOnPage ? Driver.FindElement(by) : null;
        }

        public void Close() => Driver?.Close();

        public void Quit() => Driver?.Quit();

    }
}
