﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;


namespace Framework.Waiter
{
    public class WebWaiter
    {
        private WebDriverWait wait;

        public WebWaiter(WebDriverWait wait) => this.wait = wait;

        public void VerifyTitle(string title) => wait.Until(ExpectedConditions.TitleIs(title));

        public void UntilToBeClickable(IWebElement element) => wait.Until(ExpectedConditions.ElementToBeClickable(element));

        public bool UntilToBeVisible(IWebElement element)
        {
            try
            {
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(new ReadOnlyCollection<IWebElement>(new List<IWebElement>() { element })));
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
            return true;
        }

        public bool UntilElementAppearOnPage(By by)
        {
            try
            {
                wait.Until(ExpectedConditions.ElementExists(by));
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
            return true;
        }

        public void UntilToBeVisible(By by) => wait.Until(ExpectedConditions.ElementIsVisible(by));

        public void WaitUtilTextToBePresentInElement(IWebElement element, string text) => wait.Until(ExpectedConditions.TextToBePresentInElement(element, text));

        public void WaitUntilElementDissapear(IWebElement element) => wait.Until(el => !element.Displayed);
    }
}
