﻿using System;

namespace Framework
{
    class Settings
    {
        public static TimeSpan TimeWait => TimeSpan.FromSeconds(20);
        public static string Browser => "chrome";
    }
}
