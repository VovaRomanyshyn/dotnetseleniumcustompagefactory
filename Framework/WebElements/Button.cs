﻿using Framework.Driver;
using Framework.Pages;
using OpenQA.Selenium;
using System;

namespace Framework.WebElements
{
    public class Button : BaseElement, IButton
    {
        public Button(IWebElement Element, BaseDriver Driver, string Name) : base(Element, Driver, Name)
        {
        }

        public void Click()
        {
            if (IsOnPage)
            {
                _waiter.UntilToBeClickable(_element);
                _element.Click();
                _logger.Info($"Button: {_name} made click");
            }
            else throw new NoSuchElementException($"Unable to find element {Name}");
        }

        public TPage ClickAndNavigateTo<TPage>() where TPage : IPage
        {
            if (IsOnPage)
            {
                _waiter.UntilToBeClickable(_element);
                _element.Click();
                _logger.Info($"Button: {_name} made click and navigate driver to page: {typeof(TPage).Name}");
                return (TPage)typeof(TPage).GetConstructor(new[] { typeof(BaseDriver) }).Invoke(new object[] { _driver });
            }
            else throw new NoSuchElementException($"Unable to find element {Name}");
        }
    }
}
