﻿using Framework.Driver;
using OpenQA.Selenium;
using System;

namespace Framework.WebElements
{
    public class TextInputField : BaseElement, ITextInputField
    {
        public TextInputField(IWebElement Element, BaseDriver Driver, string Name) : base(Element, Driver, Name)
        {
        }

        public ITextInputField Clear()
        {
            _waiter.UntilToBeVisible(_element);
            _element.Clear();
            _logger.Info($"TextInputField {_name} cleared string");
            return this;
        }

        public ITextInputField SendString(string str)
        {
            _waiter.UntilToBeVisible(_element);
            _element.SendKeys(str);
            _logger.Info($"Text input field: {_name} send sting: {str}");
            return this;
        }
    }
}
