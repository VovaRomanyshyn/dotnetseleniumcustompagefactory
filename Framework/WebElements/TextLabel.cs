﻿using Framework.Driver;
using OpenQA.Selenium;
using System;

namespace Framework.WebElements
{
    public class TextLabel : BaseElement
    {
        public TextLabel(IWebElement Element, BaseDriver Driver, string Name) : base(Element, Driver, Name)
        {
        }

        public override string ToString()
        {
            _waiter.UntilToBeVisible(_element);
            var Text = _element.Text;
            _logger.Info($"Text lable: {_name} contains folloing test: {Text}");
            return Text;
        }

        public string WaitUtilTextToBePresentInElement(string text)
        {
            _waiter.WaitUtilTextToBePresentInElement(_element, text);
            _logger.Info($"Text lable: {_name} contains folloing test: {text}");
            return _element.Text;
        }
    }
}
