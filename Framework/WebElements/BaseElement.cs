﻿using Framework.Driver;
using Framework.Waiter;
using NLog;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;

namespace Framework.WebElements
{
    public class BaseElement : Element, IBaseElement
    {
        protected BaseDriver _driver;
        public IWebElement _element;
        protected WebWaiter _waiter;
        public string _name;
        protected Logger _logger;

        public BaseElement(IWebElement Element, BaseDriver Driver, string Name)
        {
            _element = Element;
            _driver = Driver;
            _waiter = Driver.Waiter;
            _name = Name;
            _logger = Driver.Logger;
        }

        public RetryingElementLocator RetryingElementLocator => new RetryingElementLocator(_element, Settings.TimeWait);

        public BaseDriver Driver => _driver;

        public IWebElement Element => _element;

        public string Name => _name;


        public override string ToString() => _element.Text;

        public string VisibleString()
        {
            var IsVisible = _waiter.UntilToBeVisible(_element);
            if(IsVisible)
            {
                return _element.Text;
            }
            else
            {
                throw new NullReferenceException($"Element: {_element}  is not visible on Page");
            }
        }

        public bool IsVisible() => _waiter.UntilToBeVisible(_element);
        

        public bool IsOnPage
        {
            get
            {
                object type;
                try
                {
                    type = _element.GetType();
                    _logger.Info($"Element: {_name} is on page");
                }
                catch (NoSuchElementException)
                {
                    type = null;
                    _logger.Info($"Element: {_name} is unable on page");
                }

                if (type != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string TagValue(string tag) => _element.GetAttribute(tag);

        public TElement CastTo<TElement>() where TElement : Element
        {
            _logger.Info($"casting element to: {typeof(TElement).Name.ToString()}");
            return (TElement)typeof(TElement)
                .GetConstructor(new[] { typeof(IWebElement), typeof(BaseDriver), typeof(string) })
                .Invoke(new object[] { _element, _driver, _name });
        }

        public void MoveToElement()
        {
            if (IsOnPage)
            {
                _driver.Actions.MoveToElement(_element).Perform();
                _logger.Info($"Mouse is focused on element: {Name}");
            }
            else throw new NullReferenceException($"Element {Name} is ont attached on page");
        }

        public void WaitUntilElementDissapear()
        {
            _waiter.UntilToBeVisible(_element);
            _waiter.WaitUntilElementDissapear(_element);
        }
    }
}
