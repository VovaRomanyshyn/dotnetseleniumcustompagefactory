﻿using Framework.Driver;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Framework.WebElements
{
    public interface IBaseElement : IElement
    {
        IWebElement Element { get; }
        string Name { get; }
        RetryingElementLocator RetryingElementLocator { get; }
        BaseDriver Driver { get; }
        string VisibleString();
        bool IsVisible();
        bool IsOnPage { get; }
        string TagValue(string tag);
        TElement CastTo<TElement>() where TElement : Element;
        void MoveToElement();
    }
}
