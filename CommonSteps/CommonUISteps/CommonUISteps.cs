﻿using CommonSteps.Pages;
using Framework.Pages;
using Framework.WebElements;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace CommonSteps.CommonUISteps
{
    [Binding]
    public class CommonUISteps : PageContainer
    {
        [Given(@"User navigates to (.*) page")]
        public void UserNavigeteToPage(string Page) => NavigateTo(Page);

        [When(@"User logins with folloing credentionals user: (.*) password: (.*)")]
        public void UserLoginsWith(string User, string Pass) => Execute(Instance<ILogin>().CorrectLogin(User, Pass));

        [When(@"User clicks on (.*) (?:button|field|element)")]
        public void WhenUserCliksOnButton(string name) => Element<IButton>(name).Click();

        [When(@"User clicks on (.*) (?:button|field|element) if it visible")]
        public void WhenUserCliksOnButtonIfItVisible(string name)
        {
            var button = Element<IButton>(name);
            if (button.IsVisible())
            {
                button.Click();
            }
        }

        [When(@"User move mouse on (.*) (?:button|field|element)")]
        public void WhenUserMoveMouseOnElement(string name) => Element<IBaseElement>(name).MoveToElement();

        [When(@"User provides (.*) in (.*) field")]
        public void WhenUserInputsSring(string text, string name) => Element<ITextInputField>(name).SendString(text);

        [Then(@"User sees loaded (.*) page")]
        public void UserSeesPage(string page)
        {
            if (!CurrentPage().Name().Equals(page))
            {
                SetCurrentPage(page);
            }
            var IsLoaded = Instance<ILoad>().IsLoaded();
            Assert.That(IsLoaded, Is.True, $"Page: {CurrentPage().Name()} is loaded: {IsLoaded}");
        }

        [Then(@"Verify visibilyty of (.*) (?:button|field|element)")]
        public void ThenVerifyVisibilytyOfField(string name) => Assert
            .That(Element<IBaseElement>(name).IsVisible(), Is.True, $"Element: {name} is on page: {CurrentPage().Name()}");

        [Then(@"Verify unvisibilyty of (.*) (?:button|field|element)")]
        public void VerifyUnvisibilityOfElement(string name) => Assert
            .That(Element<IBaseElement>(name).IsVisible(), Is.False, $"Element: {name} is on page: {CurrentPage().Name()}");

        [Then(@"List of (.*) (?:buttons|fields|elements) is visible")]
        public void ThenVerifyVisibilytyOfListElements(string name)
        {
            var ElementList = ElementList<IBaseElement>(name);
            Logger.Info($"Verify if list {name} contains more than 0 elements. Count is {ElementList.Count}");
            Assert.That(ElementList.Count > 0, Is.True, $"Verify if list {name} contains more than 0 elements.");
            Logger.Info($"Verify visibilyty of list {name} of {ElementList.Count} elements.");
            foreach (BaseElement element in ElementList)
            {
                Assert.That(element.IsVisible, Is.True, $"Element {element._name} is visible on page: {CurrentPage().Name()}");
            }
        }

        [When(@"User clicks on any element from (.*) (?:buttons|fields|elements) list")]
        public void WhenUserClicksOnAnyElementFromList(string name)
        {
            var list = ElementList<IButton>(name);
            list[new Random().Next(list.Count)].Click();
        }

        [When(@"User clicks on (.*) element from (.*) (?:buttons|fields|elements) list")]
        public void WhenUserClicksOnElementFromList(int number, string name) => ElementList<IButton>(name)[number - 1].Click();

        [When(@"If drop down is visible user selects any options in (.*) drop down element")]
        public void WhenIfDropDownIsVisibleUserSelectsAnyOptionsInDropDownElement(string name)
        {
            var DropDown = Element<IDropDown>(name);
            if (DropDown.IsOnPage)
            {
                DropDown.SelectAnyOption();
            }
        }

        [When(@"User selects (.*) option from (.*) drop down element")]
        public void UserSelectsOptionFromDropDownElement(string option, string name) => Element<IDropDown>(name).ClickByText(option);

        [When(@"User clicks on (?:button|field|element) by text (.*)")]
        public void WhenUserClicksOnElementByText(string text) => ElementWithText<Button>(text).Click();

        [When(@"User clicks on (?:button|field|element) by contain text (.*)")]
        public void WhenUserClicksOnElementByContainText(string text) => ElementWithContainText<Button>(text).Click();

        [Then(@"Verify if Table (.*) row (.*) cell (.*) contains text (.*)")]
        public void ThenVerifyIfTableRowCellContainsText(string Table, int Row, int Cell, string Text)
        {
            var CellText = Element<ITable>(Table)[Row - 1][Cell - 1].CastTo<TextLabel>().VisibleString();
            Assert.That(CellText.Equals(Text), Is.True, $"Verifying if Text on {Table} Row: {Row} Cell: {Cell} is Equals {Text}");
        }

        [AfterScenario]
        public void Close()
        {
            CloseDriver();
        }
    }
}
