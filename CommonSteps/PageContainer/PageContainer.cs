﻿using Framework.Driver;
using Framework.Pages;
using Framework.WebElements;
using Model.SpecFlowPages;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommonSteps.Pages
{
    public class PageContainer
    {
        private static IPage _page;
        private static Dictionary<string, Type> _pageTypes;
        public static Logger Logger => _page.BaseDriver().Logger;

        //private PageContainer()
        //{
        //}

        public static IPage CurrentPage() => _page;

        public static void NavigateTo(string stringType) => _page = (IPage)Activator.CreateInstance(PageType(stringType));

        public static TPage Instance<TPage>() where TPage : IPage => (TPage)_page;

        public static void Execute(IPage page) => _page = page;

        public static TElement Element<TElement>(string elementName) where TElement : IElement
        {
            var fieldInfo = _page.GetType().GetField(elementName,
                BindingFlags.Instance | BindingFlags.Static |
                BindingFlags.Public | BindingFlags.NonPublic);
            if (fieldInfo is null)
            {
                throw new NullReferenceException($"Element {elementName} do not exist on page: {_page.Name()}");
            }
            return (TElement)fieldInfo.GetValue(_page);
        }

        public static IList<TElement> ElementList<TElement>(string elementListName) where TElement : IElement
        {
            var fieldInfo = _page.GetType().GetField(elementListName,
                BindingFlags.Instance | BindingFlags.Static |
                BindingFlags.Public | BindingFlags.NonPublic);
            if (fieldInfo == null)
            {
                throw new NullReferenceException($"Element {elementListName} do not exist on page: {_page.Name()}");
            }
            var proxy = fieldInfo.GetValue(_page);
            var list = proxy as IEnumerable;
            return list.Cast<TElement>().ToList();
        }

        public static TElement ElementWithText<TElement>(string text) where TElement : IBaseElement
        {
            var element = _page.ElementWithText<TElement>(text);
            if (element == null)
            {
                throw new NullReferenceException($"Element with text: {text} do not exist on page: {_page.Name()}");
            }
            return element;
        }

        public static TElement ElementWithContainText<TElement>(string text) where TElement : IBaseElement
        {
            var element = _page.ElementWithTextContaints<TElement>(text);
            if (element == null)
            {
                throw new NullReferenceException($"Element with text: {text} do not exist on page: {_page.Name()}");
            }
            return element;
        }

        internal static void SetCurrentPage(string stringType) => _page = (IPage)PageType(stringType)
                .GetConstructor(new[] { typeof(BaseDriver) })
                .Invoke(new object[] { _page.BaseDriver() });

        private static Type PageType(string stringType) => PageTypes()[stringType];

        private static Dictionary<string, Type> PageTypes()
        {
            if (_pageTypes is null)
            {
                InitPageTypes();
            }
            return _pageTypes;
        }

        private static void InitPageTypes()
        {
            
            var type = typeof(SpecFlowPage);
            var pageTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));

            var dictioary = new Dictionary<string, Type>();
            foreach (Type t in pageTypes)
            {
                dictioary.Add(t.Name, t);
            }
            _pageTypes = dictioary;
        }

        public static void CloseDriver()
        {
            if (_page != null)
            {
                _page.BaseDriver().Close();
                _page.BaseDriver().Quit();
            }
        }

        public static BaseDriver BaseDriver => _page.BaseDriver();
    }
}
