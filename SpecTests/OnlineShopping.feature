﻿Feature: OnlineShopping

@shopping
Scenario: User shold choose the size of before add to cart on Adidas Page
	
	Given User navigates to AdidasMainPage page
	Then User sees loaded AdidasMainPage page
	When User clicks on CloseCookies button

	When User move mouse on Men element
	Then List of MenShoes buttons is visible
	When User clicks on 2 element from MenShoes buttons list
	Then User sees loaded AdidasShoesListPage page
	
	When User selects Najpopularniejsze option from SortBy drop down element
	When If drop down is visible user selects any options in PageNumberDropDown drop down element
	And User clicks on 5 element from ShoesList fields list
	Then User sees loaded AdidasShoePage page

	When User clicks on AddToCart button
	Then List of Sizes elements is visible


@shopping1
Scenario: User shold choose the size of before add to cart on Reebok Page
	
	Given User navigates to ReebokMainPage page
	Then User sees loaded ReebokMainPage page
	When User clicks on CloseCookies button

	When User move mouse on Men element
	Then List of MenShoes buttons is visible
	When User clicks on 1 element from MenShoes buttons list
	Then User sees loaded ReebokShoesListPage page

	When User selects Najpopularniejsze option from SortBy drop down element
	When User clicks on 3 element from ShoesList fields list
	Then User sees loaded ReebokShoePage page

	When User clicks on AddToCart button
	Then List of Sizes elements is visible