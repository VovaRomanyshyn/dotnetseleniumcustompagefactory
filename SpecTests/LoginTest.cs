﻿using Framework.Pages;
using Model.w3schools;
using NUnit.Framework;

namespace UnitTests
{
    class LoginTest
    {
        private IPage page;

        [Test]
        public void TableTest()
        {
            //Arrange
            page = new W3Schools();
            //Act
            var result = ((W3Schools)page).CustomersTable[2][2].VisibleString();
            //Assert
            Assert.True(result.Equals("Mexico"));
        }

        [TearDown]
        public void Close()
        {
            page.BaseDriver().Close();
        }

    }
}
